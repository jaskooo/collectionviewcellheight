//
//  ViewController.m
//  CollectionViewCellHeight
//
//  Created by Jan Lipmann on 25.02.2015.
//  Copyright (c) 2015 Likomp. All rights reserved.
//

#import "ViewController.h"
#import "CollectionViewCell.h"
#import "UIView+Nib.h"
#import "UIImage+autoHeight.h"

static NSString * const kCellId = @"kCellId";

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:kCellId];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 5;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellId forIndexPath:indexPath];
    NSString *key = [NSString stringWithFormat:@"%@",@(indexPath.row)];
    NSString *text = NSLocalizedString(key, nil);
    cell.textLabel.text = text;
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",@(indexPath.row + 1)]];
    [cell setImageHeight];
    return  cell;
}

+ (CGFloat)heightForText:(NSString*)text font:(UIFont*)font withinWidth:(CGFloat)width {
    CGSize size = [text sizeWithAttributes:@{NSFontAttributeName:font}];
    CGFloat area = size.height * size.width;
    CGFloat height = roundf(area / width);
    return ceilf(height / font.lineHeight) * font.lineHeight;
}

// In this method you really shouldnt create everytime cell just to calculate sizes. Its removing idea of reusable cells in collection view.
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0;
    NSString *key = [NSString stringWithFormat:@"%@",@(indexPath.row)];
    NSString *text = NSLocalizedString(key, nil);

    CGFloat textHeight = [[self class] heightForText:text font:[UIFont systemFontOfSize:18] withinWidth:collectionView.frame.size.width - 60]; // 60 is margins
    height += textHeight;
    
    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",@(indexPath.row + 1)]];
    
    CGFloat imageHeight = [image scaleImageAutoLayoutWithWidth:CGRectGetWidth(self.collectionView.frame)];
    height += imageHeight;
    
    height += 30; //main view margins
    height += 15; //uiimage view margins
    
    return CGSizeMake(CGRectGetWidth(self.collectionView.frame), height);
}

@end
