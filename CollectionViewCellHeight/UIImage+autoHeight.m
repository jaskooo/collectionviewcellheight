//
//  UIImage+autoHeight.m
//  CollectionViewCellHeight
//
//  Created by Bartosz Hernas on 26.02.2015.
//  Copyright (c) 2015 Likomp. All rights reserved.
//

#import "UIImage+autoHeight.h"

@implementation UIImage(autoHeight)

- (CGFloat)scaleImageAutoLayoutWithWidth:(CGFloat)width
{
    CGFloat imageWidth = self.size.width;
    CGFloat imageHeight = self.size.height;
    CGFloat scale = imageHeight / imageWidth;
    CGFloat height = width * scale;
    return height;
}

@end
