//
//  UIView+Nib.h
//  Kolibree
//
//  Created by Jan Lipmann on 01.07.2014.
//  Copyright (c) 2014 Jan Lipmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Nib)
+ (instancetype)loadFromNibNamed:(NSString *)nibName;
@end
