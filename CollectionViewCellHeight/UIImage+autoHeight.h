//
//  UIImage+autoHeight.h
//  CollectionViewCellHeight
//
//  Created by Bartosz Hernas on 26.02.2015.
//  Copyright (c) 2015 Likomp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (autoHeight)
- (CGFloat)scaleImageAutoLayoutWithWidth:(CGFloat)width;
@end
