//
//  CollectionViewCell.h
//  CollectionViewCellHeight
//
//  Created by Jan Lipmann on 25.02.2015.
//  Copyright (c) 2015 Likomp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
- (void)setImageHeight;
@end
