//
//  CollectionViewCell.m
//  CollectionViewCellHeight
//
//  Created by Jan Lipmann on 25.02.2015.
//  Copyright (c) 2015 Likomp. All rights reserved.
//

#import "CollectionViewCell.h"
#import "UIImage+autoHeight.h"

@implementation CollectionViewCell

- (void)awakeFromNib {
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}


- (void)setImageHeight {
    CGFloat width = CGRectGetWidth(self.frame) - 30; // you cannot use here self.imageView.frame since this view hasnt "layouted" yet and its frame is wrong.
    
    CGFloat imageHeight = [self.imageView.image scaleImageAutoLayoutWithWidth:width];
    
    self.heightConstraint.constant = ceilf(imageHeight);
    [self setNeedsUpdateConstraints];
    [self setNeedsLayout];

}

@end
