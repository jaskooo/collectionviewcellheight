//
//  SizableLabel.m
//  CollectionViewCellHeight
//
//  Created by Jan Lipmann on 25.02.2015.
//  Copyright (c) 2015 Likomp. All rights reserved.
//

#import "SizableLabel.h"

@implementation SizableLabel

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    
    if ( self.numberOfLines == 0 ) {
        self.preferredMaxLayoutWidth = self.bounds.size.width;
        [self setNeedsUpdateConstraints];
    }
}

@end
