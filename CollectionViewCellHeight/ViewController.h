//
//  ViewController.h
//  CollectionViewCellHeight
//
//  Created by Jan Lipmann on 25.02.2015.
//  Copyright (c) 2015 Likomp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;

@end

