//
//  UIView+Nib.m
//  Kolibree
//
//  Created by Jan Lipmann on 01.07.2014.
//  Copyright (c) 2014 Jan Lipmann. All rights reserved.
//

#import "UIView+Nib.h"

@implementation UIView (Nib)
+ (instancetype)loadFromNibNamed:(NSString *)nibName {
    id customView = [[[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[self class]]) {
        
//        [customView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
        return customView;
    } else {
        return nil;
    }
}
@end
